#!/bin/bash

# It is simple script to install VAPT tool. 
# This script will install the clair scanner db, clair scanner local server and clair scanner command your machine.
# To know more about clair scanner db and clair scanner local server go to https://github.com/arminc/clair-local-scan


# Global variables  
declare -f flag
re='^[0-9]+$'

# Constant variables
NC='\033[0m'

# Function will validate the network_name(global varaible) with the list of networks running in docker.  
# flag=0:          Indicates no error,
# flag=1:          Indicates error
check_network_name(){
    # take out the list of networks running in docker 
    declare -A network_names=$(sudo docker network ls --format "{{.Name}}") 
    local i=0
    flag=0
    # Validates network_name in network_names array. If network_name is in network_names array then flag is set to 1 and error message is assign to network_output( global variable ) 
    for i in ${network_names[@]}; do
    	if [ "$network_name" == "$i" ]; then
    		flag=1
    		network_output="duplicate_network_name"
    		break
    	fi
    done
}

# Function will create a new network on docker
# flag=0:          Indicates no error,
# flag=1:          Indicates error
create_network(){
	# This function will check the availabilty of name in list of networks in docker.
	check_network_name 
	# Check the flag which is set by check_network_name function. If flag is set to 0 it means that the network name as a input given by a user can be use to create docker network. If flag is set to 1 then running_options with argument( network_output ) function is called. 
	if [[ $flag == 0 ]]; then
		output=$(sudo docker network create $network_name >/dev/null)
	    if [[ $? == 0 ]]; then
			echo -e "$(tput setaf 2)Successfully created $network_name network in docker${NC}"
		else
			echo "Error:"
			echo $output
			exit 1
		fi	
	else
		running_options $network_output
		
	fi
	
}
    
# Function will check the port availability.
# flag=0:          Indicates no error,
# flag=1:          Indicates error
check_port_availability(){
	# This command will check whether the port given by user as a input is already occupied or not. If it is then it will set flag to 1 and error message is assign to port_output(global variable). And if port in not occupied then flag is set to 0. 
	sudo  netstat -tulpn |  awk '{print $4}' | grep $port >/dev/null
    # Check the status of above command. If exit status of above is equals to 0 that means port given by user is occupied by another process. If exit status is 1 then flag is set to 0, this implies that user entered port is free to use.     
    if [[ $? == 0 ]]; then
    	flag=1
    	port_output="duplicate_port"
    else
    	flag=0
    fi
}

# Function will check the name availability in docker. It will validate any docker container has 
# occupied the container_name(given by user) or not  
# flag=0:          Indicates no error,
# flag=1:          Indicates error
check_docker_container_name(){
    # take out the list of containers name in docker. 
    declare -A container_names=$(sudo docker ps -a --format "{{.Names}}") 
    local i=0
    flag=0
    # Validates container_name in container_names array. If container_name is in container_names array then flag is set to 1 and error message is assign to container_name_output( global variable )
    for i in ${container_names[@]}; do
    	if [[ $container_name == $i ]]; then
    		flag=1
    		container_name_output="duplicate_container_name"
    		break
    	fi
    done
}

# Function will check both port and container_name availablity. Internally to check port it will call
# check_port_availablity function and to check container name it will call check_docker_container_name function
# flag=0:          Indicates no error,
# flag=1:          Indicates error
check_port_and_docker_container_name(){
	# First, it will check the port availability.
	check_port_availability 
	if [[ $flag == 0 ]]; then
		# If above condition is true then check_docker_container_name function is called and it 
		# will check the container_name availability.    
        check_docker_container_name 
        # If flag is equal to 1 then it will declare common_output varaible with 
	    # container_name_output which is declared and initialized in check_docker_container_name function.
		if [[ $flag == 1  ]]; then
			common_output=$container_name_output	
		fi
	else
		common_output=$port_output
	fi
}

# Function will create the clair db container in docker. If any error occurs during creation, it will give an error  
# as a output 
# flag=0:          Indicates no error,
# flag=1:          Indicates error
create_clair_db(){
	# First it will go to check_port_and_docker_container_name function to check the container_name and port number availability. 
	check_port_and_docker_container_name 
	# In check_port_and_docker_container_name function flag will set to either 0 or 1. If it is 0 then it will create
	# a container with the given container name and port.  
	if [[ $flag == 0 ]]; then
		output=$(sudo docker run -p $port:5432 -d --net=$network_name --name $container_name arminc/clair-db:$(date -d "yesterday" '+%Y-%m-%d') >/dev/null)
		# If the status of above command equals to 0 then it will print out success message with conatiner name 
		if [[ $? == 0 ]]; then
			db_container_name=$container_name
			echo -e "$(tput setaf 2)$container_name docker container successfully created ${NC}"
			flag=0
		else
			echo "Error:"
			echo "$output"
			exit 1
		fi
	else
		# If flag is equals to 1 in check_port_and_docker_container_name function then running_options function is called with common_output as an argument. 
		running_options $common_output 1
			 
	fi
	
}
# Function will create the clair local server container in docker. If any error occurs during creation, it will give an error
# as a output
# flag=0:          Indicates no error,
# flag=1:          Indicates error
create_local_clair_server(){
	# First it go to check_port_and_docker_container_name function to check the container_name and port number availability. 
	check_port_and_docker_container_name
	# In check_port_and_docker_container_name function flag will set to either 0 or 1. If it is 0 then it will create
	# a container with the given container name and port.
	if [[ $flag == 0 ]]; then
		output=$(sudo docker run -p $port:6060  --net=$network_name --link $db_container_name:postgres -d --name $container_name arminc/clair-local-scan:v2.0.1 >/dev/null)
		# If the exit status of above command equal to 0 then it will print out success message with conatiner name
		if [[ $? == 0 ]]; then
			echo -e "$(tput setaf 2)clair local server container successfully created${NC}"
			flag=0
		else
			echo -e "$(tput setaf 1)Error:"
			echo "$output${NC}"
			exit 1
		fi
	else
		# If flag is equal to 1 in check_port_and_docker_container_name function then running_options function is called with common_output as an argument.  
		running_options $common_output 2
			
	fi
}

# Function will help user to assign correct port_number, network_name and container_name, with appropriate options.
# Input Argument1 : option,
# Input Argument2 : number   
running_options() {
   	# The option variable will contain anyone of the following values.
   	# 1. duplicate_container_name
   	# 2. duplicate_port
   	# 3. duplicate_network_name
   	local option=$1
   	# As to identify which function has called running_options function, number variable is assign with a single digit number.
   	# There can be only two number ( 1 or 2 )
   	# 1 indicates that running_option function is called from create_clair_db function.
   	# 2 indicated that running_option function is called from create_local_clair_scanner function.   
    local number=$2
   		case "$option" in
   			"duplicate_container_name" )
				echo -e "\nDocker container with same name running on machine"
				read -p "Choose different name for docker container: " container_name
				if [[ $number == 1 ]]; then
					create_clair_db 
				else
					create_local_clair_server 
				fi
				
   			;;
   			"duplicate_port" )
			echo -e "\nPort is already occupied. Kill the process or map the different port to container. Choose a option \n"
            while :; do
            	
				echo "1: Exit from the program and kill process who has occupied $port port"
				echo -e "2: Map the container to a different port\n"
		        read -p "Write you option here. It should either '1' or '2' nothing else: " option
		        if [[ $option == 1 ]]; then
		        	echo "Exiting "
		            exit 0
		        elif [[ $option == 2 ]]; then
		        	read -p "Enter the port you want to map to container: " port
		            if [[ $number == 1 ]]; then
						create_clair_db 
					else
						create_local_clair_server 
					fi
		            break
		        else 
		        	echo -e "\n${RED}Wrong option, you have to choose either 1 or 2${NC}"
		        	sleep 6
		        fi
            done
				
		    ;;
		    "duplicate_network_name" )
                echo -e "\n"
				read -p "Network name already exist. Enter different network name: " network_name
				create_network $network_name
   		esac
   	

}

# Function will download clair-scanner_linux_amd64 from the web and export its path to the PATH varaible. 
clair_scanner_installation(){
	echo "Clair_scanner will be installed in current file"
	# Assigning the current path of user 
	current_path=$(pwd)
	# Check whether the clair-scanner_linux_amd64 is already there in users machine or not
	compgen -ac | grep clair-scanner_linux_amd64 >/dev/null
	# If exit status of above command is 0 then it will print the clair scanner already available. But, if exit status of command is 1 
	# then it will download the clair scanner and export its path to the users PATH variable
	if [[ $? -eq 0 ]]; then
		echo "clair-scanner already avialable"
	else
		# It will go to the users home directory
		cd ~ >/dev/null
		# Create a clair-scanner folder under which clair-scanner_linux_amd64 wil be downloaded.
		mkdir clair-scanner >/dev/null
		cd clair-scanner >/dev/null
		# Takes the path where clair-scanner_linux_amd64 has been downloaded 
		path=$(pwd)
		# changes the directory to the current path
		output=$(sudo wget https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64)
		# If exit status of above command equal to 0, then the clair-scanner_linux_amd64 is made executable by chmod command. 
		# If exit status equal to 1 then it will print the error message. 
		if [[ $? == 0 ]]; then
	 		sudo chmod +x clair-scanner_linux_amd64
	        export PATH=$PATH:$path/
	        cd ~ >/dev/null
	        compgen -ac | grep clair-scanner_linux_amd64 >/dev/null
	        # If above command exit status is equal to 0 then a success message is printed. But, if status is equal to 1 then
	        # User have to manually add the path of clair-scanner_linux_amd64 in PATH varaible. 
	        if [[ $? -eq 0 ]]; then
	        	cd $current_path >/dev/null
	        	echo "Successfully included clair_scanner in PATH"
	        	echo -e "Type clair_scanner command in your terminal"
	        else
	        	cd $current_path >/dev/null  	
	        	echo "Manual add the path of clair-scanner_linux_amd64 in PATH variable${NC}"
	        fi 
	 	else
	 		echo "$(tput setaf 1)Error:"
			echo "$output${NC}"
		fi 
	fi
	
}



echo -e "\n****************Launching Setup to install clair scanner************************ \n"


read -p "Enter the network name: " network_name
create_network $network_name
echo -e "\n"

while :; do
	read -p "Enter the db port number[Press enter for default(5432)]: " port
	if [ -z $port ]; then
		port=5432
		break
	elif ! [[ $port =~ $re ]]; then
		echo -e "\n Entered value is not a number"
	else 
		break
	fi	
done
echo -e "\n"
read -p "Enter the container name of db: " container_name
create_clair_db $port $container_name
echo -e "\n"
while :; do
	read -p "Enter the clair server port number you want to map you container[Press enter for default(6060)]: " port
	if [ -z $port ]; then
		port=6060
		break
	elif ! [[ $port =~ $re ]]; then
		echo -e "\nEntered value is not a number\n"
	else
		break
	fi	
done

echo -e "\n"
read -p "Enter the container name of clair server: " container_name
create_local_clair_server $port $container_name

echo -e "\nSetting up the clair-scanner command to your machine"
clair_scanner_installation

